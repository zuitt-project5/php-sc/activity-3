<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<title>s3: Activity 3</title>
</head>
<body>
	<h1>Person</h1>
	<p><?= $person->printName(); ?></p>

	<h1>Developer</h1>
	<p><?= $dev->printName(); ?></p>

	<h1>Engineer</h1>
	<p><?= $eng->printName(); ?></p>

</body>
</html>